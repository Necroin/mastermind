package klem.wedoogift.mastermind;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Guess {
    private int turnCount;
    private String input;
    private List<Token> tokens = new ArrayList<>();
    private int perfectMatches;
    private int colorMatches;
    private boolean evaluated = false;

    public Guess(String input, int turnCount) {
        this.turnCount = turnCount;
        this.input = input.toUpperCase();
    }


    public int getTurnCount() {
        return turnCount;
    }

    public String getInput() {
        return input;
    }

    public List<Token> getTokens() {
        if (this.tokens.isEmpty()) {
            this.tokens = Collections.unmodifiableList(parse(input));
        }
        return tokens;
    }

    private List<Token> parse(String input) {
        return input.chars()
                .mapToObj(i -> Token.fromChar((char) i))
                .collect(Collectors.toList());

    }


    public void setPerfectMatches(int perfectMatches) {
        this.perfectMatches = perfectMatches;
    }


    public void setColorMatches(int colorMatches) {
        this.colorMatches = colorMatches;
    }

    public int getColorMatches() {
        return colorMatches;
    }

    public int getPerfectMatches() {
        return perfectMatches;
    }

    public boolean isEvaluated() {
        return evaluated;
    }

    public void setEvaluated(boolean evaluated) {
        this.evaluated = evaluated;
    }

    @Override
    public String toString() {
        return String.format("|%s| %d | %d | %d/%d |",
                input, perfectMatches, colorMatches, turnCount, App.TRIES);
    }
}
