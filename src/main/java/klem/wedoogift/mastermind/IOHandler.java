package klem.wedoogift.mastermind;

import java.util.Scanner;

public class IOHandler {

    Scanner scanner;

    public IOHandler() {
        this.scanner = new Scanner(System.in);
    }

    public void write(String prompt, Object... args) {
        System.out.println(String.format(prompt, args));
    }

    public String read() {
        return scanner.next();
    }
}
