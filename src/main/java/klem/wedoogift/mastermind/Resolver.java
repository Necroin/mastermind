package klem.wedoogift.mastermind;


import java.util.*;


public class Resolver {

    private List<Token> answer ;
    private ArrayList<Token> answerCpy;

    public Resolver(int servingSize) {
        this.answer = Token.serve(servingSize);
    }

    public Resolver(List<Token> answer) {
        this.answer = answer;
    }

    public boolean evaluate(Guess guess) {
        this.answerCpy = new ArrayList<>(answer);

        int perfectMatches = findPerfectMatches(guess);

        boolean solved = perfectMatches == answer.size();

        if(!solved) {
           findColorMatches(guess);
        }

        guess.setEvaluated(true);

        return solved;
    }

    private int findPerfectMatches(Guess guess) {
        int match = 0;
        Iterator<Token> guessIt = guess.getTokens().iterator();
        Iterator<Token> answerIt = answer.iterator();

        while (guessIt.hasNext()) {
            Token g = guessIt.next();
            Token a = answerIt.next();
            if(g.equals(a)) {
                match++;
                answerCpy.remove(a);
            }
        }
        guess.setPerfectMatches(match);
        return match;
    }

    private int findColorMatches(Guess guess) {
        int match = 0;
        for (Token g : guess.getTokens()) {
            if (answerCpy.contains(g)) {
                answerCpy.remove(g);
                match++;
            }
        }
        guess.setColorMatches(match);
        return match;
    }

    public List<Token> getAnswer() {
        return answer;
    }


    @Override
    public String toString() {
       return answer.toString();
    }
}
