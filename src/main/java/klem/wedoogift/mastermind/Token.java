package klem.wedoogift.mastermind;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public enum Token {
    ROUGE('R'),
    JAUNE('J'),
    BLEU('B'),
    ORANGE('O'),
    VERT('V'),
    NOIR('N');

    private char c;

    Token(char c) {
        this.c = c;
    }

    public char getChar() {
        return c;
    }

    static Token fromChar(char c) {
        Token found = null;
        for (Token t : Token.values()) {
            if (t.getChar() == c) {
                found = t;
                break;
            }
        }

        if(found == null) {
            throw new IllegalArgumentException(String.format("Illegal Input %s", String.valueOf(c)));
        }

        return found;
    }

    public static List<Token> serve(int servingSize) {
        List<Token> toServe = new ArrayList<>();

        final Token[] availableTokens = Token.values();

        for (int i = 0; i < servingSize; i++) {
            Random r = new Random();
            toServe.add(i, availableTokens[r.nextInt(Token.values().length)]);
        }

        return toServe;
    }

    public static List<Token> serve(String tokens) {
        List<Token> toServe = new ArrayList<>();

        int i = 0;
        for (Character c : tokens.toCharArray()) {
            toServe.add(i, fromChar(c));
            i++;
        }

        return toServe;
    }

}
