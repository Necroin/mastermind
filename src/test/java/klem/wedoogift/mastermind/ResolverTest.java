package klem.wedoogift.mastermind;

import klem.wedoogift.mastermind.Guess;
import klem.wedoogift.mastermind.Resolver;
import klem.wedoogift.mastermind.Token;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class ResolverTest {

    @Test
    public void testServingFromSize() {

        int servingSize = 4;

        Resolver r = new Resolver(servingSize);

        Assertions.assertThat(r.getAnswer().size()).isEqualTo(servingSize);
        Assertions.assertThat(Token.values()).contains(r.getAnswer().toArray(new Token[servingSize]));


    }

    @Test
    public void testServingFromString() {

        final String bjno = "BJNO";

        Resolver r = new Resolver(Token.serve(bjno));

        Assertions.assertThat(r.getAnswer().size()).isEqualTo(bjno.length());
        Assertions.assertThat(Token.values()).contains(r.getAnswer().toArray(new Token[bjno.length()]));


    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalCharachterInServing() {

        final String bxno = "BXNO";
       new Resolver(Token.serve(bxno));

    }

    @Test
    public void guessEvaluatedButNotSolvedTest() {

        final String bjno = "OOOO";

        Resolver r = new Resolver(Token.serve(bjno));
        Guess guess = new Guess("NNNN", 0);

        Assertions.assertThat(guess.isEvaluated()).isFalse();

        final boolean solved = r.evaluate(guess);

        Assertions.assertThat(guess.isEvaluated()).isTrue();
        Assertions.assertThat(solved).isFalse();


    }

    @Test
    public void fullPerfectMatchTest() {

        final String bjno = "BJNO";

        Resolver r = new Resolver(Token.serve(bjno));
        Guess guess = new Guess(bjno, 0);


        final boolean solved = r.evaluate(guess);
        Assertions.assertThat(solved).isTrue();
        Assertions.assertThat(guess.getPerfectMatches()).isEqualTo(r.getAnswer().size());
        Assertions.assertThat(guess.getColorMatches()).isEqualTo(0);


    }

    @Test
    public void fullColorMatchTest() {

        final String answer = "BJNO";
        final String userInput = "JBON";

        Resolver r = new Resolver(Token.serve(answer));
        Guess guess = new Guess(userInput, 0);

        final boolean solved = r.evaluate(guess);
        Assertions.assertThat(solved).isFalse();
        Assertions.assertThat(guess.getPerfectMatches()).isEqualTo(0);
        Assertions.assertThat(guess.getColorMatches()).isEqualTo(r.getAnswer().size());

    }

    @Test
    public void noMatchTest() {

        final String answer = "BBBB";
        final String userInput = "JJJJ";

        Resolver r = new Resolver(Token.serve(answer));
        Guess guess = new Guess(userInput, 0);

        final boolean solved = r.evaluate(guess);

        Assertions.assertThat(solved).isFalse();
        Assertions.assertThat(guess.getPerfectMatches()).isEqualTo(0);
        Assertions.assertThat(guess.getColorMatches()).isEqualTo(0);

    }

    @Test
    public void partialPerfectAndColorMatchTest() {

        final String answer = "BJBJ";
        final String userInput = "BJJB";

        Resolver r = new Resolver(Token.serve(answer));
        Guess guess = new Guess(userInput, 0);

        Assertions.assertThat(guess.isEvaluated()).isFalse();

        final boolean solved = r.evaluate(guess);

        Assertions.assertThat(solved).isFalse();
        Assertions.assertThat(guess.isEvaluated()).isTrue();
        Assertions.assertThat(guess.getPerfectMatches()).isEqualTo(2);
        Assertions.assertThat(guess.getColorMatches()).isEqualTo(2);

    }

    @Test
    public void partialPerfectMatchTest() {

        final String answer = "BJBJ";
        final String userInput = "BJOO";

        Resolver r = new Resolver(Token.serve(answer));
        Guess guess = new Guess(userInput, 0);

        Assertions.assertThat(guess.isEvaluated()).isFalse();

        final boolean solved = r.evaluate(guess);

        Assertions.assertThat(solved).isFalse();
        Assertions.assertThat(guess.isEvaluated()).isTrue();
        Assertions.assertThat(guess.getPerfectMatches()).isEqualTo(2);
        Assertions.assertThat(guess.getColorMatches()).isEqualTo(2);

    }
}
